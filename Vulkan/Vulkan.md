A new generation graphics _and_ compute API 
Enables high efficiency by being low-level
A very explicit (maybe verbose API)
Cross-platform and cross-device

Vulkan has a very thin driver which only handles explicit GPU control, compared to OpenGL where the driver implements a high-level state machine abstraction over the GPU.

![[Pasted image 20240321221414.png]]

Derived from the Mantle API (by AMD and DICE) for the PlayStation 4 and Xbox One
Created, maintained, and evolved by Khronos Group