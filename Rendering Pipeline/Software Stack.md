# The Application

This is the application. You either statically link to the 

# The API Runtime

This is where the resource creation / state setting / draw calls to the API occur. The API runtime keeps track of the state that the application has created, where validation and error checking (implicitly in OpenGL but explicitly with layers in Vulkan), managing user-visible resources, and other work occurs. In Linux this is (probably) the `vulkan-1.so` library and what connects applications to the user-space driver - the *user space driver loader*.

# The User Space Driver (UMD)

This is the majority of the work on the CPU side occurs. This is user-mode code which runs in the same address space as the application and the API runtime. This is where a majority of OpenGL, older version of DirectX (more specifically the DDI, which is fairly similar to D3D), and Vulkan (less so than OpenGL) APIs are implemented. 

The UMD is the module where shader compilation occurs for OpenGL and D3D. In Vulkan the shaders are compiled and built externally into binary [[SPIR-V]] blobs. High level, hardware agnostic optimizations (various loop optimizations, dead-code elimination, constant propagation, predicating ifs etc.) are applied to the shader program before being sent to the GPU. Low level optimizations (such as register allocation and loop unrolling) can also be applied at this stage, but these often require knowledge of the hardware that the drivers would rather implement themselves. The optimized versions of the programmable shaders are then turned into an IR format.

Where OpenGL, D3D, and Vulkan converge is that after shader compilation the IR for the shader is compiled further into a binary for the GPU. For OpenGL and D3D, these compilers are baked into the driver packages and called during the application's run-time.

Some API state can be baked into the shader program, e.g., texture boarders and push constants. This causes multiple versions of the same shader program to exist since they exist statically in the binary. This is why games stutter when loading / creating a new shader or resource - the compilation work is differed by the driver and only executed when necessary.

This is why applications like [[Fossilized]] exist - to capture the graphics / render pipeline state for future use so that hardware and software does not need to reproduce this work. This is also an advantage for Vulkan [[SPIR-V]] blobs - the shader programs are compiled before the application executed, saving more run-time costs.

The UMD also handles memory management. The UMD provides the API runtime for objects such as textures and command buffers are sub-allocated from a large memory block received from the kernel-mode driver (KMD). 

The UMD is also responsible for scheduling swapping between video and host memory (and consequentially responsible for memory coherency), and writing to command buffers once the KMD has allocated memory for them. All of the API state and drawing operations are written as commands to the command buffer, as well as other automatic operations such uploading textures.

As stated earlier, as much of the graphics APIs are implemented in the UMD as possible. No need for kernel-mode transition costs, freely allocating memory, connecting to multiple applications and threads (more on this in the KMD section), preventing a whole system crash, and debugging with a regular debugger are some of the advantages.

On Linux, technologies such as [[Gallium3D]] exist in the user-mode driver, and other periphery libraries such as [[GBM]], [[EGL]], and [[Window System Integration]] also exist as user-space libraries.

The user-mode drivers in Linux are just dynamic libraries under `/lib64/`. For OpenGL it's usually `/lib64/libGL.so` irrespective of the vendor. For Vulkan it's usually `/lib64/libvulkan.so` and `/lib64/libvulkan_*.so`, where the `*` is the name of the driver that provides the interconnect to the KMD.
## OpenGL

For OpenGL, there is not a sharp distinction between the API and the UMD layers and the shader compilation is all done in the UMD - unlike in D3D where it is handled in the API layer specifically and Vulkan where it's precompiled (from the driver's perspective).

# The Kernel Mode Driver (KMD)

The Kernel-Mode Driver (KMD) is rather simple since most of the work is done in the UMD. The KMD is the sole owner of the GPU resources and is responsible for multiplexing and orchestrating access to applications.

The first responsibility of the KMD is the GPU scheduler - it's responsible for time slicing access to the graphics hardware across multiple applications using it. When changing program is running on the GPU, a context switch, state switching, and resource switching all occur in the KMD.

The KMD deals with GPU memory - as mentioned earlier. The KMD is responsible for allocation large blocks of virtual memory for the UMD to use for graphics objects, mapping them to physical memory, as well as mapping and unmapping virtual pages to and from video memory. It also has a hardware watchdog timer in case the GPU becomes unresponsive.

More importantly, the KMD manages the *actual* command buffer. The command buffers created by the user-mode driver are just slices of host and/or GPU memory. What actually happens is that the UMD finishes writing to it's command buffers and submits them to the scheduler. When it's time to run that process, the KMD then writes a call to the main command buffer to start executing that UMD's command buffer. The main command buffer is a (usually quite small) ring buffer  - the only things that are written to it are system / device initialization commands and calls to the UMD command buffers.

The main command buffer for the command processor is pretty simple. There is a read pointer where the command processor is currently executing commands from, and a write pointer ahead of the read pointer to how far the KMD has written to it so far.

For Vulkan in Linux, the KMD is usually `amdgpu` and is baked into the Linux kernel as a [[Kernel Dynamic Module Support]] module.

# The Bus

Command buffers don't magically appear on GPU memory from the host. Depending on whether or not the GPU can read from main memory or not, it may also need to DMA it to video memory first. This is usually the PCI Express bus now-a-days.

# The Command Processor

Front-end of the GPU - the component that actually executes the commands the KMD provides to it.


# Resources

- [A Trip Through the Graphics Pipeline - Part 1](https://fgiesen.wordpress.com/2011/07/01/a-trip-through-the-graphics-pipeline-2011-part-1/)
- [Fossilize Repository](https://github.com/ValveSoftware/Fossilize)