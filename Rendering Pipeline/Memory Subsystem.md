This was mentioned in the [[Software Stack#The Kernel Mode Driver (KMD)|The Kernel Mode Driver]] section that there is a Direct Memory Access (DMA) transfer of memory from the host to the GPU memory. This process and the memory model of the GPU is more complex than that and will be explored here

# The Memory Subsystem

The hardware of a GPU has a different memory system than general-purpose CPUs since it was designed for a very different usage pattern.

First of all, the GPU memory system has incredibly high bandwidth. While general-purpose CPUs can hit tens of GB/s of memory bandwidth (68 GB/s on DDR5 5600 MHz memory), a GPU (with discrete memory) can hit hundreds of not terabytes of GB/s (a 3080 TI has 918 GB/s of bandwidth).

The caveat is that GPU memory also has incredibly high latency. A cache miss on GPU memory is a few times if not tens of times slower than a general-purpose CPU. Graphics (and compute) hardware is all about *throughput* over latency.

Other than these pieces of information, the only other salient piece of information to know is that, because of this focus on throughput over latency, packing data into the GPU's cache line is incredibly valuable. This will be explored later.

# The PCI Express Host Interface

The PCI Express (PCIe) host interface is not the most interesting piece of the graphics pipeline for programmers, but it's helpful to be aware about (unless the PCIe bus becomes a bottleneck itself, but there's not much a programmer can do about that).

This interconnect is what gives the CPU read / write access to GPU memory and a few GPU registers, as well as giving the GPU access to (some of) the main memory. Passing data through this bus is *even slower* than accessing GPU memory with less throughput (~ 2 GB/s per lane in PCIe 4).

# Resources
- [A Trip Through The Graphics Pipeline - Part 2](https://fgiesen.wordpress.com/2011/07/02/a-trip-through-the-graphics-pipeline-2011-part-2/)