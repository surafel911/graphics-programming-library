
# Pipeline Overview
These are the components of the graphics pipeline. These are the stages that raw data goes through to become pixels on a display.

<img src="https://vulkan-tutorial.com/images/vulkan_simplified_pipeline.svg">

## Vertex Input

The vertex input is a stage in the graphics pipeline, but a required input to the graphics pipeline. The vertex input specifies the various details about vertex buffers and the vertices themselves.
- Vertex Binding Descriptions describes the buffers themselves and how their used. This can be the binding number, stride / pitch of the different vertices, and input rate
- Vertex Attribute Descriptions describe about the contents of the buffer. It defines the binding locations (i.e., the layouts in the shaders), the format of the data (`vec3` or `vec4`), and offsets to different attributes that may be interleaved in the buffer (e.g, position, color, and normal values are interleaved in one vertex buffer).
## Input Assembler

Collects the raw vertex (and index) data from the buffers the application specified. The input assembler uses a data cache to exploit cache locality to access vertices and indices. Bounds checking occurs in this stage, i.e., throwing an error when an index count 6 from the command buffer was given on a 5 index buffer, or referencing a non-existent vertex in the index buffer. The instance counter is also created which just an increment and fairly straightforward.

The input assembler is responsible for unpacking the data in the data-layout specified in the command buffers and packaging it into a float format for the shader programs (topology is also used in packaging the data for shaders). The data isn't read from the cache immediately though, if an already shaded vertex is used in multiple triangles, the graphics pipeline will reuse the shaded vertex from the cache. 

## Vertex Shader

The vertex shader is a programmable shader stage that is run for every vertex and generally applies transformations to turn vertex positions into model space, then world space, clip space, then finally to screen space. The processed vertex data is passed to the next stage.

## Primitive Assembler

The primitive assembler (not documented) groups the vertex data (transformed by the vertex shader) into primitives. The type of primitives that are assembled is specified by the application when creating the graphics pipeline.

## Tessellation Shader

The tessellation shader is a programmable shader stage allows you to subdivide geometry based on certain rules to increase mesh quality. This is often useful to increase the detail of surfaces / models based on how close the camera.

## Geometry Shader

The geometry shader is a programmable shader stage that is run on every primitive (triangle, line, point). The geometry shader can transform geometry or even increase or decrease the number of primitives. This is similar in scope to the tessellation shader but much more flexible. However, the performance of geometry shaders are not the best so be cautious of the GPU's capabilities.

## Viewport and Scissor

Although the viewport and scissor mechanic is **not** a shader stage, it is important to discuss this step since it is critical to the next stage (rasterizer). The upcoming stages in the graphics pipeline need to have appropriate bounds on what vertices to keep and discard. 

The viewport and scissor tests take the unbounded view from the previous stages, and will clip the vertices into a final "viewable" region. The viewport and scissor are two separate regions, and the intersection of the region will (commonly) become the "view" for the final framebuffer.

## Culling

Culling is the process of selectively rendering or excluding triangles in a scene that are not visible to the camera. This is done by observing the whether the triangle faces away (called back-face culling) or towards (called front-face culling) the camera.

In the render pipeline, the programmer can specify which face to cull as well as the orientation of the front face vertices (either clockwise or counter-clockwise). For example, if the vertices of a front facing triangle are given in clockwise order and back face culling is enabled, then triangles whose vertices are draw in a counter-clockwise order are culled.
## Rasterization

Rasterization is a non-programmable stage that discretizes the primitives into *fragments* - the pixel elements that fill the framebuffer. Any fragments that fall outside of the screen are discarded and the outputs of the vertex shader are interpolated across the fragments. Depth testing occurs here and fragments of primitives that appear behind fragments of other primitives are discarded here as well.

Rasterization a solution to the *visibility* problem. There are many other solutions to the visibility problem (the only other relevant one being ray tracing).

### The Visibility Problem

The visibility problem is figuring out how to be able to tell which parts of a primitive (as well as which primitive) is visible to the camera. As mentioned earlier, the two main methods to solve the visibility problem are ray tracing and rasterization.

#### The Ray Method 

The ray method (i.e., ray tracing) to solving the visibility problem involves tracing a ray from the camera through every pixel in the image to find out the distance between the camera and any object this ray intersects (if any). The object with the smallest distance is denoted as the visible object. In this case, you loop through all the pixels in the image for tracing the ray and another loop to figure out if the ray intersects any primitive (basically O(n^2)). The ray method is referred to as **image-centric** because it relies on shooting rays "through" the image into the scene

### Rasterization

The other approach is an **object-centric** method where the 3D object is "projected" to a 2D representation of that primitive using the perspective projects (i.e., the `w` component of the 4D vector). The next step is finding out which pixels are covered by the 2D projection of that primitive.

The algorithm is relatively much simpler than the ray method.

## Early Depth Testing (Early Z)

Depth testing is the process of determining how far each fragment is from the camera, and discarding fragments that both overlap *and* is further away than the former fragment. Depth testing can occur both before, and after the fragment shader.

The benefits of early z testing is that discarding geometry is much cheaper before processing the fragments. This is useful for Z-Culling (also called occlusion culling), where geometry that is completely occluded by another piece of geometry closer to the camera is discarded. This is useful for complex map geometry.

## Fragment Shader

The fragment shader is a programmable stage that is invoked for every fragment (that survives) in the framebuffer. Note that fragments are not the same as pixels since it's (still) possible that fragments may still be obscured in the next stage.

The fragments are written into the final image with a color, depth value, texture coordinate (i.e., samples), and other data that are interpolated (from vertex to fragment) from the vertex shader.

## Late Depth Testing (Late Z)

Late Depth Testing is similar to early z testing, the difference is that this occurs after the fragment shader. 

## Color Blending

The color blending stage is a non-programmable stage that applies operations to mix fragments that map to the same pixel in the framebuffer. Fragments can simply overwrite each other, add up or be mixed based upon transparency.

The output of the color blending stage is the final framebuffer that is rendered.

# Shaders
## Shader Units
Shaders are just programs, and programs must run on real hardware. There aren't many resources on how *exactly* shader units work since those are trade secret details, but there are numerous high-level overviews of how shader units work.

Shader units are built on fast Arithmetic Logic Units (ALU) build on Floating Multiply-Accumulate (FMAC) units. Shaders programs operate mostly with floating point values, ergo shader units are designed around operations with floating point numbers. There is also hardware support for common math operations such as square root, sin, cos, log, and exponential. One important detail is that shader units are deigned for throughput, not low-latency. Latency is (partially) handled by the shear amount of parallelism in GPUs.

Since there are many shader units, there are often very few registers. Often times multiple clusters of shader units will share instruction and data pointers in the cluster control block. This is perfectly fine since GPUs are intended to run serial applications. However, this makes branches incredibly slow on GPUs, since the GPU will have to wait for shader units on *each side* of the branch to finish executing before continuing with the program.

## Unified Shader Architecture
Shaders for different stages in the days of old were not only separate programs, but each stage had a different set of capabilities and instructions. Vertex shaders had more instructions and capabilities than pixel (i.e., fragment in today's terms) shaders since vertex shaders dealt with linear transformations compared to pushing fragments to the screen. This reduced overall hardware costs in early graphics.

Since OpenGL 3.3, the Unified Shader Architecture was introduced. This design meant that all shader processing units were capable of handling any type of shader task. The Unified Shader Architecture meant that graphics hardware is composed of an array of shader units and some form of dynamic loader / load balancing system that kept all computational units as busy as possible.

The Unified Shader Architecture allows the hardware to be more flexible. For example, in cases with a heavy vertex / geometry load, more shader units can be allocated to executing vertex and geometry shaders to properly saturate the pipeline.