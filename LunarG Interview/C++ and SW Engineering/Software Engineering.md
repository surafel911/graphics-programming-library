# Engineering

## SDLC
The software development life cycle (SDLC) is a cost-effective and time-efficient development processes developers use to create high-quality software. The goal is the SDLC is to minimize risk by planning ahead so the software meets the customer's requirements in all phases of production and deployment.

Benefits of the SDLC:
- Increased participation of stakeholders in development process
- Efficient estimation, planning, and scheduling
- Reduces risks and wasted cost of improper communication
- Systemic software delivery approach for customer satisfaction

Stages of the SDLC:
1. **Planning** - Gather business requirements, costs and benefits analysis, and goals of the project. Results in the **Software Specification Document** (SSD).
2. **Requirements** - Analysis of the SSD; critical phase where requirements are generated information from the SSD. Includes high and low level requirements, function and non-functional requirements. This phase produces the **Software Requirements Specification** (SRS) and must be understood by stakeholders *and* developers.
3. **Design** - Requirement gathered in the SRS are used to derive the software architecture  for implementing the software system. This includes high-level design such as  integrating preexisting modules, making technology choices, and identify development tools, as well as lower level choices such as design patterns.
4. **Implementation** - Development of the software product. Analyze the requirements and decompose them into high-level modules then further decompose them to low-level components.
5. **Testing & Integration** - Development team utilizes manual and automation testing to look for bugs and validate that the product is meeting the requirements in the SRS document. Refer to [[Software Engineering#Testing|Testing]] for more info about the types of tests. More time and care should be spent here since the next phase is where the most development resources are spent.
6. **Deployment & Maintenance** - Team fixes bugs, resolves customer issues, and manages software changes (i.e., validate/improve functional requirements of the product) as well as testing performance, security, and user-experience (i.e., validate/improve non-functional requirements).

![SDLC](https://civenty.com/uploads/content_image/60c378a947a2a777822625.png)

### Agile Methodology

The Agile methodology arranges the phases of the SDLC into development cycles. As an iterative and incremental process, the team begins with a small set of requirements then incrementally enhances the software over multiple versions. At the end of each iteration, the development team meets with stakeholders to repeat the process.

Iterations are referred to as *sprints*, which lasts 2-4 weeks. At the end of each sprint, the team and stakeholders meet to verify the product is meeting the requirements and is on schedule.

The team iterates through the phases rapidly, delivering only small, incremental software changes in each cycle. They continuously evaluate requirements, plans, and results so that they can respond quickly to change.

The advantages of Agile are:
- It allows more flexibility to adapt to the changes.
- The new feature can be added easily.
- Customer satisfaction as the feedback and suggestions are taken at every stage.

The disadvantages of Agile are:
- Lack of documentation.
- Agile needs experienced and highly skilled resources.
- If a customer is not clear about how exactly they want the product to be, then the project would fail.

![[agile-methodology-diagram.png]]

# Development Practices

## Debugging

**Clarify the problem**
1. What did you expect your code to do?
2. What happened instead?
 
 **Examine your assumptions**
 1. Do you know the intent of the code?
 3. Are you using the API correctly?
 4. Did you expect an object to have a certain value?
	
**Use a debugger to step through your code**
There are different debugging techniques one can employ to find a bug
1. Scan and look
2. "`printf`" debugging
3. "Binary-search debugging"
	
Note that debugging is a majority of developer time, and is (often) a sign of poor testing. Thorough testing and requirements analysis can (oftentimes) reduce the amount of debugging

Can make use of tools such as `rr` (Record and Replay)
## Maintainability

Some guidelines on writing maintanable code:
1. Adopt a coding standard / style
2. Adopt a particular language version
3. Adopt code reviews for MR 
4. Collaborate with other developers (e.g., pair testing / development)
5. Use linter(s) whenever possible (preferably in CI/CD pipelines)
6. Use automated testing (e.g., test driven development)
7. Adopt popular policies like KISS, DRY, and SOLID when writing code
8. Avoid hard-coded values and macros; utilize const variables to explain what the value is   
   Follow the advice from Clean Code by Robert Martin:
   1. Meaningful names
   2. Functions Should Do One Thing
   3. Short Functions
   4. Clean/Clear Comments
   5. Proper Error Handling
   6. Code Formatting and Organization
   7. Test-Driven Development

## Testing

SDLC V-Model
![SDLC V-Model](https://i0.wp.com/melsatar.blog/wp-content/uploads/2018/08/Screen-Shot-2018-08-28-at-2.19.21-PM.png?resize=768%2C501&ssl=1)

### Unit Testing
Testing one specific piece of code / logic in a program
1. [[Black Box Testing]] - Testing the interface / usage of a specific piece of code / logic
	1. Equivalence Partitioning - Testing different values from separate sets of possible input data. E.g., if a piece of logic accepts an `int`, test passing positive numbers, negative numbers, and 0.
		2. Boundary Value Analysis - An extension to Equivalence Partitioning testing; test the boundary values from each partition.
		3. Requirements Testing - Design tests that specifically test whether requirements are met (i.e., tests that feature works)
			1. Positive Tests - Tests that check whether product does what it's supposed to do
			2. Negative Tests - Tests that check whether product does not fail when given unexpected inputs
		4. Error Guessing - Pass test data that is likely to expose faults in the code. E.g., `NULL`, empty strings, white-space, negative numbers (if positive numbers are required), etc.
		5. Domain Testing - Have "domain people" perform tests by using the software; business flow determines test, not "logic" or "steps"
2. [[White Box Testing]] - Tests the program by mapping code to functionality
	1. Static Testing - Code reviews / inspections. Can be done by linters in CI/CD.
		1. Structural Testing - Tests done by running the the code, the objective is to maximize *code coverage*
			1. Statement Coverage - Writing tests that execute all statements in code 
			2. Path coverage - Writing tests that execute all potential execution paths in code; testing all possible paths in the program
			3. Conditional Coverage - Writing test that executes each conditional in boolean expression is covered
			4. Function Coverage - Writing tests that executes all functions in program
### Integration Testing 
Testing the interfaces between modules. Two main types of interfaces:
1. Internal interfaces - interfaces provided for use by other developers / components
2. External interfaces - interfaces provided for use by other systems / products

There are two main methods to integration testing:
1. Top-down approach - higher level modules are tested before lower level modules; a more system-centric approach for high-level functionality and use cases that's useful for high-level / requirements testing evaluating system behavior
2. Bottom-up approach - lower level modules are tested before higher level modules; a component-centric approach that's useful for early error detection and parallel development


## Concurrency and Parallelism:
1. Explain the concept of multithreading in software development. What are the challenges associated with it?
    - How do you approach and handle race conditions in concurrent programming?



# Data Oriented Design
https://github.com/dbartolini/data-oriented-design
https://www.youtube.com/watch?v=uzF4u9KgUWI

The main principles of data-oriented design are:
1. Minimizing data movement: Moving data around a program is expensive, therefore aim to minimize moving data around. This means using references or move semantics.
2. Optimizing for cache (data locality): Processors use a cache to temporarily store data, which has access times much faster than main memory. Organize and pack data to utilize the cache effectively

How to use data-oriented design:
1. Use contiguous memory / data structures. Prefer `array` then `vector`, and only deviate if you need 
2. Grouping related data together to avoid jumping between memory locations
3. Use simple data structures such as arrays and structs whenever possible
4. Use structure of arrays (SoA) instead of an array of structures when possible.
5. Use const-correctness and move semantics for optimizations

Considerations when writing code following the data oriented design principles
1. Think about data first, and code second. Class hierarchies aren’t important, but data access patterns are.
2. Think about how data in your game is accessed, how it is transformed, and what you end up doing with it, e.g. particles, skinned characters, rigid bodies, and tons of other examples.
3. When there’s one, there’s many. Think in streams of data.
4. Be aware of the overhead of virtual functions, pointers to functions, and pointers to member functions.

Richard Fabian in his Data Oriented Design book recommends avoiding pointers since 1) they are a level of indirection and 2) can represent a boolean value whether the pointer is `NULL` or not. Therefore avoiding pointers when possible can simplify code

# Software Design

## System Design
1. Discuss the process of designing a scalable and efficient system for a graphics-related application.
    - Explain how you would optimize the performance of a graphics rendering algorithm.
## Design Principles
### SOLID
- Single Responsibility Principle (SRP)
	A class should have one reason to change; each class should have a single responsibility and should encapsulate that responsibility
- Open-Closed Principle (OCP)
	Classes should be open for extension but closed for modification
- Liskov Substitution Principle (LSP)
	  A derived class should be able to substitute its base class without causing any unexpected behavior
- Interface Segregation Principle (ISP)
	Clients should not be forced to depend on interfaces they do not use
- Dependency Inversion Principle (DIP)
	  High-level modules should not depend on low-level modules. Both should depend on abstractions, i.e. *use interfaces*

### KISS
Shorthand for "Keep It Simple Stupid". Implement KISS by using the following principles:
- Set clear expectations
- Use simple design patterns
- Embrace simplicity in code

### DRY
Shorthand for "Don't Repeat Yourself". The DRY principle is states as: "Every piece of knowledge must have a single, unambiguous, authoritative representation within a system".

The way to achieve DRY is by creating functions and classes to make sure that any logic should be written in only one place. That one function and class must be the sole owner of that representation

### YAGNI
Shorthand for "You Aren't Going to Need It". Extreme Programming (XP) practice which states: "Always implement things when you actually need them, never when you just foresee that you need them."

## Design Patterns
3. **Design Patterns**
	1. **Singleton Pattern:**
	    - **Use Case:** Ensuring a single instance of a driver or a kernel module.
	    - **Example:** Creating a singleton class to manage global resources or configuration settings.
	2. **Factory Method Pattern:**
	    - **Use Case:** Instantiating objects based on specific conditions, configurations, or device types.
	    - **Example:** A factory method that creates different device drivers based on the type of hardware.
	3. **Observer Pattern:**
	    - **Use Case:** Notifying multiple components about changes or events in the system.
	    - **Example:** Implementing an observer pattern for asynchronous notification of events within a driver.
	4. **Command Pattern:**
	    - **Use Case:** Encapsulating operations as objects, allowing for queuing, logging, or undo functionality.
	    - **Example:** Representing driver commands as objects to support command queuing or logging.
	5. **Adapter Pattern:**
	    - **Use Case:** Providing a uniform interface for different classes or systems.
	    - **Example:** Adapting a driver interface to comply with a standard kernel API or accommodating changes in hardware specifications.
	6. **Proxy Pattern:**
	    - **Use Case:** Controlling access to an object or adding additional functionality without modifying its core behavior.
	    - **Example:** Implementing a proxy class to manage access to specific hardware resources or to log interactions.
	7. **Decorator Pattern:**
	    - **Use Case:** Dynamically adding responsibilities to objects without altering their code.
	    - **Example:** Decorating driver functionality with additional features such as logging or performance monitoring.
	8. **Strategy Pattern:**
	    - **Use Case:** Defining a family of algorithms, encapsulating each one, and making them interchangeable.
	    - **Example:** Using different scheduling strategies for handling device interrupts or managing data transmission.
	9. **State Pattern:**
	    - **Use Case:** Allowing an object to alter its behavior when its internal state changes.
	    - **Example:** Representing different states of a device (e.g., power-off, standby, active) and transitioning between them.
	10. **Command Processor Pattern:**
		- **Use Case:** Managing sequences of operations or commands in a driver.
		- **Example:** Implementing a command processor to handle and coordinate complex interactions within a driver.