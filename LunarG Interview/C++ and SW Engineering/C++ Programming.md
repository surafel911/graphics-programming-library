https://erkaman.github.io/posts/junior_graphics_programmer_interview.html
https://www.jeremyong.com/graphics/interviewing/2023/08/05/graphics-programmer-interviewing/
https://www.glassdoor.com/Interview/Intel-Corporation-Graphics-Software-Engineer-Interview-Questions-EI_IE1519.0,17_KO18,44.htm

# Basics
1. Explain the differences between C++11, C++14, and C++17. Provide examples of features introduced in each version.
   
   https://github.com/AnthonyCalandra/modern-cpp-features
   
   C++11:
   - Unified initialization, that allowed for a constant syntax for initializing a variety of data structures. Consequentially also introduces initializer lists.
   - `auto` and `decltype` keywords are added to let the compile automatically deduce the types. `auto` lets the compiler deduce the type of a variable based on an `rvalue` and `decltype` lets the compiler create a variable based on the type of an expression (e.g., function call). 
   - Smart pointers were introduced in C++11 for automatic heap memory management and ownership semantics. Differences explained later.
   - C++ introduced a memory model which means the standard library has support for threading and atomic types and operations
   - Multithreading support was introduced in C++11. Previously applications needed to use POSIX threads (`pthreads`) library. The `std::thread` object is introduced that implements a POSIX-like threading API for C++ (note concurrency comes later). Asynchronous calls  are introduced with `std::async` that allow the programmer to run a function asynchronously and wait for the return value at a later point.
   - Move semantics and constructor defined the two types of references: `lvalue` and `rvalue` references. Was introduced to to "move" or change ownership of objects. This also included the introduction of forwarding (universal) references.
   - Lambda functions, i.e. inline functions for short snippets of code were added in C++ 11.
   - Hash (i.e., unordered) containers were added in the C++11. The other versions of these containers sorted by the key value, not by the hash of the key.
   - Type traits defines a compile-time template-based interface to modify the property of value of types (e.g., `is_same<T, K>, is_class<T>`, `is_reference`, etc.).
   - `constexpr` is a keyword added to increase the performance of computations by calculating the value at compile-time not run-time.
   - Other features like type aliasing (`using Vec = std::vector<T>`) and `nullptr` (type safe `NULL`), strongly typed enums (`enum class`) were introduced in C++11.
C++14:
- Binary literals, i.e. a convenient way to express binary numbers.
- Generic lambda expressions allows C++14 programs to use the `auto` keyword to generalize lambda functions. For example in sorting functions ` [](auto i, auto j) -> bool { return i > j; }`
- Lambda capture initializers let the lambda capture be initialized with expressions, such as a function call: `[x = f(2)] mutable { x = 10; }`
- Restrictions applied to `constexpr` are removed. E.g., using `++i`, having only one return statement, if statements and loops, etc.
- Return type deduction is added in C++14 where the programmer can use `auto` as the return type.
- Adds the ``[[deprecated]]`` attribute to produce a compiler warning that a class, struct, and/or function is intended to be deprecated.

2. Describe the use cases for `const` in C++ and how it differs from `constexpr`.
	   `const` declares an _object_ as having a _constant value_. This is a guarantee in _initialization_, where the compiler can make optimizations and gives the programmer explicit about the mutability of a variable.
	   
	`constexpr` declares that an object _and function_ for use in the compiler's _constant expressions_. A constant expression allows the compiler to evaluate the value of the function at compile-time. E.g.,  a fixed size list template size (like `std::array`) will use `constexpr` to return the fixed size at compile-time.
   
3.  What are smart pointers in C++? How do the various smart pointers implementations differ?
	   Smart pointers are an RAII wrapper for heap allocated memory that provide automatic memory management (based on the lifetime of the wrapper object) and ownership semantics to allocated memory. Smart pointers solve memory leaks, dangling pointers, wild pointers, and buffer overflow issues with normal C pointers.
	   
	   `std::unique_ptr`: allows exactly one owner of the underlying resource. Ownership can be transferred but not referenced nor shared.
	   
	   `std::shared_ptr`: is a referenced counted smart-pointer to share ownership of the underlying resource. The resource is not freed until all owners relinquish ownership of the smart pointer (i.e., reference count goes to 0).
	   
	   `std::weak_ptr`: special case smart pointer used with `std::shared_ptr` that provides access to a `std::shared_ptr` resource but does not participate in reference counting. Used to observe object but does not require it to remain alive.

4.  What is polymorphism?
	   https://www.udacity.com/blog/2021/07/understanding-polymorphism-in-cpp.html
	   
	   Polymorphism is a feature of OOP where you can have different types accessible through the same interface  
	   
	   Compile-time Polymorphism: function and operator **overloading**. Run-time Polymorphism: function and operator **overriding** from **inheritance**, and **virtual functions**

5.  What are virtual functions?  
	   A base class member function that you can redefine in a derived class for polymorphism. Are dynamic in nature and called during run-time by looking into the class’ static VTABLE (a VPTR points that points to the class' VTABLE is silently inserted as a data member in the class instance).  
	   
	   Limitations: slower to execute and difficult to debug 
	   
	   All functions in a class can be virtual except for the constructor. It is best behavior to separate the _implementation_ details and the _usage_ of an interface for proper separation of concerns, i.e. have _protected_ _virtual functions_ for implementation details and _public non-virtual_ functions for interface details  
  
6. When should you use virtual destructors?  
	Useful when potentially deleting a derived class instance through a base class pointer. W/o a virtual destructor, the destructor for the derived class may never be called resulting in a resource leak.

7. Encapsulation vs abstraction  
	Encapsulation means enclosing related data & methods into a single entity. Can be used to hide / protect data and behaviors from a user. This is as simple as putting data in a class.
	
	Abstraction is a means of **_generalization_** - taking a concrete implementation and making it applicable to different, yet related, types of data. Abstraction is more akin to creating an interface / abstract class or using templates than creating a class.
	
	Encapsulation and abstraction often go hand-in-hand so it may be difficult to separate. Encapsulation is a _structural_ kind of abstraction

# Memory Management
1. Discuss the differences between stack and heap memory in C++.
   
   Stack:
   - Linear data structure
   - High speed access to data
   - Fast allocations (simply moving the stack pointer)
   - Will never become fragmented; High locality of reference
   - Accesses local variables in the same (or higher) lexical scope
   - Cannot be resized; size is known at compile-time
   - Memory is allocated in a contiguous block
   - Memory allocation and deallocation is automatic and done by compiler instructions
Heap:
- Hierarchical data structure
- Low speed access to data
- Slower allocations (manipulating internal data structures)
- Can become fragmented; low locality of reference
- Can access variables globally (i.e., the memory is not limited to the current scope)
- Heap variables can be resized
- Allocation is in any random order can be implemented using arrays and trees)
- Memory allocation and deallocation are manual and done by the programmer
   
2. Discuss the differences between `new` and `malloc` in C++.
	1. `new` is an operator in C++ while `malloc` is a function
	2. `new` is type-safe (returns the data type allocated) while `malloc` always returns `void*`
	3. On failure, `malloc` returns `NULL` while `new` returns an exception
	4. Size requirements are required for `new`, `malloc` accepts any number of bytes
	5. Can resize buffer allocated using `malloc` with `realloc`, not true for `new`
	6. `new` calls the constructors of data types, `malloc` does not
	7. `new` and `delete` use the free-store, while `malloc` and `free` use the heap (difference is basically conceptual and to reiterate the two memory mechanisms families are not interchangeable ).
	
	In C++ it's better to use `new` /`delete` over `malloc`/`free` for the following benefits:
	- They're safer - throws exceptions on failure
	- They're type safe - Let's compiler allocate sizes automatically and general type safety
	- They're customizable - `new` and `delete` operators can be overloaded to change allocation behavior, e.g., allocate from memory pool

3. Explain the concept of RAII and provide examples of how it is used.
	Resource Acquisition Is Initialization (RAII) is an idiom where resource acquisition / allocation is done in the constructor of a class, and resource disposal / deallocation is done in the destructor of a class.
	
	RAII is a mechanism to express "ownership" over a resource. An owner of a resource can allocate, update, free, and move data; **no other object** can manipulate or handle a resource without going through the owner.

# Data Structures and Algorithms

https://github.com/gibsjose/cpp-cheat-sheet/blob/master/Data%20Structures%20and%20Algorithms.md

## Data Structures

![data-structure-complexity](https://github.com/gibsjose/cpp-cheat-sheet/raw/master/General/Data%20Structures.png)

### Containers
**Stack**
Last In First Out (LIFO) data structure. You *push* and *pop* from the top of the stack (think of it as a *stack* of plates).

**Queue**
First In First Out (FIFO) data structure. You *push* to the "back" of the queue and *pop* from the "front" of the queue.

**Deque**
Stands for a Double Ended Queue. it's a cross b/w a stack and a queue that provides O(1) insert and deletion from both ends and (at most) O(n) indexing.

### Binary Trees

A binary tree is a data structure with the following definitions:
1. A special node called the root node,
2. Has two child nodes, the left subtree and right subtree
3. The left and right subtrees are binary trees

Insertion, deletion, and lookup operations require traversing the binary tree. There are three ways to traverse a binary tree:
1. Inorder Traversal
   - Traverse the left subtree
   - Visit the node
   - Traverse the right subtree
2. Preorder Traversal
   - Visit the node
   - Traverse the left subtree
   - Traverse the right subtree
3. Postorder Traversal
   - Traverse the left subtree
   - Traverse the right subtree
   - Visit the node

#### Binary Search Trees
A binary search tree is the same as a binary tree with the following conditions:
1. The value in the root node is larger than every key in the left subtree
2. The value in the root node is smaller than every key in the right subtree
3. The left and right subtrees are binary search trees

#### Red Black Tree
Red-Black tree is a binary search tree in where every node is colored either red or black. It is a type of self balancing binary search tree.

A tree is a Red-Black tree if it satisfies all the following properties:
1. Every node is either red or black.
2. Every leaf is black.
3. If a node is red, then both its children are black.
4. Every simple path from a node to a descendant leaf contains the same number of black node

Property #3 implies that on any path from the root to a leaf, red nodes must not be adjacent.  
However, any number of black nodes may appear in a sequence.
## Algorithms
![algorithms-complexity](https://jojozhuang.github.io/assets/images/uncategorized/9721/sorting_algorithms.png)
https://www.youtube.com/watch?v=6hC9IxqdDDw&
- `std::map` and `std::set` are almost always implemented as R/B trees
- Open addressing hash tables offer better cache performance than C++ standard `map`, `set`, and `unordered_map` containers
- Caching is not the only reason why STL hash tables are slow - STL hashing uses the modulo operator which is _incredibly slow_
# Advanced C++ Concepts
1.  Explain the use of move semantics and provide a real-world scenario where they are beneficial.
	Move semantics allow C++ programmers to change the ownership of a given object. Say for example in a constructor for a `string` object, you might use move semantics in a constructor `string{sring&& s)` to take ownership of the data inside of the `rvalue` `string` object.
	
	This method of exchanging ownership of resources is good for 1) turning expensive copies into cheap moves and 2) implementing safe "move-only" types (`std::unique_ptr`)

2.  Explain the purpose and use of lambda expressions in C++.
	Lambda expressions are a convenient way to define anonymous functions in C++. They can increase readability when for small code snippets for things like callback functions

3. Explain the Rule of Five, Rule of Three, and Rule of Zero
	   The Rule of Five are rules of thumb in C++ to producing exception-safe code and formalizing the rules on resource management. The Rule of Five states that if a user-defined class allocates a resource, it likely requires an implementation for the following functions:
	1. Destructor
	2. Copy constructor
	3. Copy assignment operator
	4. Move Constructor
	5. Move assignment operator
	 
	 For RAII applications, it's important to implement all of these operations to prevent errors / leaks. Not implementing move semantics is not necessarily an error if the class does not require it.
	 
	 The Rule of Three is simply the first three rules, before the inclusion of move semantics.
	 
	 The Role of Zero simply states that if there are no resources allocated in a user-defined class, then no user-defined implementation is required for that class (i.e., use `default` on the functions.

1. What is the diamond problem and how do you solve it
	The diamond problem is refers to an inheritance hierarchy when a class inherits from two base classes that have a common base class (i.e., inheritance diagram looks like a diamond). This results in ambiguity in which "instance" of the shared super-class is being used as well as overall bad software practice.
	   
	There are multiple ways to solve this. The first is to use **virtual Inheritance:**  to ensure that there is only one instance of the common base super-class. The other is to quality which of the parent class' functions to use.
	
	The "real" answer is to evaluate why there is a need for this many degrees of inheritance. Attempt to flatten the hierarchy as much as possible - aggregation vs composition and SOLID principles

# STL (Standard Template Library)
1. Discuss the differences between `std::vector`, `std::list`, and `std::map`. When would you choose one over the other?
	I feel comfortable answering this question
2.  How do iterators work in C++? Provide an example of using iterators with a container.
	I feel comfortable answering this question
3.  How does `std::function` work, and when would you choose it over function pointers or lambdas?
	 I feel comfortable answering this question

# Templates and Metaprogramming
1. Explain the concept of template metaprogramming and provide an example of its application.
	Where the programmer uses _templates_ that are used compiler to auto-generate code that is built with the original source code. The template metaprogramming permits the programmer to write code with templates once and the compiler will generate that same code for the defined type. Useful for containers and expressions where the type / number of values is generic but known at compile time
	
2. Explain the concept of RTTI (Run Time Type Information) and provide an example of how it is used. 
	RTTI is a C++ mechanism that lets the type of the object to be determined at run-time. It allows the type of the of an object to be determined at run-time, including run-time / dynamic casts and retrieving type information.

# Concurrency and Multithreading
1. What are the challenges of multithreading in C++? How do you avoid race conditions?
	- Increased Complexity
	- Difficult to debug and identify errors
	- Difficult to test
	- Unpredictable results
	- Synchronization
	- Sharing resources
	
	A programmer can avoid race conditions by accessing the resource atomically. This can be done by using one (or more) of several synchronization mechanisms: mutexes,  fences, semaphores, and atomics.
	
	Another method (following the Go paradigm) is instead of "communicating by sharing resources, share resources by communicating".

2.  Discuss the use of `std::mutex` and `std::atomic` in C++ for ensuring thread safety.
   
   

# Performance Optimization
1. Describe techniques for optimizing C++ code for performance, especially in the context of resource-intensive applications.
	   https://www.youtube.com/results?search_query=cppcon+performance
	   
	1. Use appropriate data structures: prefer `std::vector` and `std::array` over `std::list` for cache efficiency
	2. Optimize loops
	3. Reduce function calls
	
	You can improve the performance of any C++ program by simply doing _less work_. This means more efficient algorithms or improving performance with data structures. Following [[Software Engineering#Data Oriented Design|Data Oriented Design]] principles will largely improve performance of programs. 
	


# Code Review and Debugging
1. Describe your approach to code reviews in a C++ project. What common issues do you look for?
	   
	   https://www.youtube.com/watch?v=YzIBwqWC6EM

# Code Problems
1. Write a function for string reversal without using any library function
	```cpp
	#include <iostream>
	
	std::string& reverse(std::string& s) {
		for (size_t i = 0; i < s.length() / 2; ++i) {
			char tmp = s[i];
			s[i] = s[s.length() - 1 - i];
			s[s.length() - 1 - i] = tmp;
		}
		
		return s;
	}
	
	int main(int argc, char* argv[]) {
		(void)argc;
		(void)argv;
		
		std::string s = "Hello world!";
		std::cout << s << " : " << reverse(s) << '\n';
		
		return 0;
	}
	```

2. Write a linked List class that performs basic operations on a linked list.
	```cpp
	#include <iostream>
	#include <memory>
	#include <utility>
	
	namespace {
	template <typename T>
	struct Node {
		T _data;
		std::unique_ptr<Node> _next;
		
		Node() = default;
		
		Node(T& data) noexcept : _data {data}, _next {nullptr} {
			std::cout << "Constructing " << data << '\n';
		}
		
		Node(T& data, std::unique_ptr<Node>&& next ) noexcept : _data {data}, _next {std::move(next)} {
		std::cout << "Constructing " << data << '\n';
		}
		
		Node(T&& data) noexcept : _data {std::move(data)}, _next (nullptr) {
			std::cout << "Constructing " << data << '\n';
		}
		
		Node(T&& data, std::unique_ptr<Node>&& next) noexcept : _data {std::move(data)}, _next {std::move(next)} {
			std::cout << "Constructing " << data << '\n';
		}
		
		Node(const Node& node) = delete;
		Node& operator=(const Node& node) = delete;
		
		Node(Node&& node) : _data {std::move(node.data)}, _next {std::move(node._next)} { }

		Node& operator=(Node&& node) noexcept { std::move(node); return this; }
		
		~Node() { std::cout << "Destroying " << this->_data << '\n'; };
	};

	template <typename T>
	class List {
	private:
		std::unique_ptr<Node<T>> _head;
		
	public:
		List() = default;
		~List() = default;
		
		void push(T& value) noexcept {
			T& v { value};
			
			this->push(std::move(v));
		}
		
		void push(T&& value) noexcept {
			std::unique_ptr<Node<T>> tmp { std::make_unique<Node<T>>(value) };
			
			if (this->_head == nullptr) {
				this->_head = std::move(tmp);
				return;
			}
			
			tmp->_next = std::move(this->_head);
			
			_head = std::move(tmp);
		}
		
		void pop() noexcept {
			if (this->_head == nullptr) {
				return;
			}
			
			std::unique_ptr<Node<T>> tmp { std::move(this->_head )};
			this->_head = std::move(tmp);
		}
	};
	}
	int main(int argc, char* argv[]) {
		(void)argc;
		(void)argv;
		
		::List<int> l;
		for (int i = 0; i < 20; ++i) {
			l.push(i);
		}
	}
	```

3. Create your own sizeof function
	```cpp
	#include <iostream>
	
	namespace {
		template <typename T> constexpr size_t t_sizeof() noexcept {
			return sizeof(T);
		}
	}
	int main(int argc, char* argv[]) {
		(void)argc;
		(void)argv;
		
		std::cout << "int: " << t_sizeof<int>() << '\n';
		std::cout << "bool: " << t_sizeof<bool>() << '\n';
	}
	```

4. Reverse a linked list
	```cpp
	class Solution {
		public:
		ListNode* reverseList(ListNode* head) {
			if (head == nullptr) {
				return nullptr;
			}
			
			if (head->next == nullptr) {
				return head;
			}
			
			ListNode* current = head;
			ListNode* previous = nullptr;
			ListNode* next = nullptr;
			
			while (current) {
				next = current->next;
				current->next = previous;
				
				previous = current;
				current = next;
			}
			
			return previous;
		}
	};
	```

5. Binary Search
	```cpp
	class Solution {
	public:
		int search(vector<int>& nums, int target) {
			int left = 0, right = nums.size() - 1, middle = 0;
			while (left <= right) {
				middle = left + ((right - left) / 2);
				
				if (nums[middle] == target) {
					return middle;
				}
				
				if (target < nums[middle]) {			
					right = middle-1;
				} else if (target > nums[middle]) {
					left = middle+1;
				}
			}
			return -1;
		}
	};
	```

6. Validating a BST
	https://www.enjoyalgorithms.com/blog/validate-binary-search-tree

7. Remove nth element from end of list
	```cpp
	
	class Solution {
	public:
		ListNode* removeNthFromEnd(ListNode* head, int n) {
			if (!head) {
				return head;
			}
			
			ListNode dummy(0, head);
			
			ListNode *left = &dummy, *right = head;
			
			for (int i = 0; i < n; ++i) {
				right = right->next;
			}
			
			while (right) {
				right = right->next;
				left = left->next;
			}
			
			left->next = left->next->next;
			
			return dummy.next;
		}
	};
	```