- blending

- dot and cross product (How to use and where they are typically used)
- matrix/vector math
- scale, translate, and rotation matrices
- model, view, projection matrices
- Orthogonality
- matrix reflections (tf is this?)

- how to find performance bottleneck

# Graphics Pipeline

[[Graphics Pipeline]]
## Buffer Types

### Uniform Buffers
Remains uniform during the execution of the command (e.g., draw call). The buffer is "read-only".  This data can only be loaded but not written to, which allows the GPU to cache efficiently that might not be useful for other buffer types.

The buffer is stored in GPU memory is loaded into L2 then L1 cache during command execution. The buffer can even be loaded into multiple L1 caches if multiple clusters of core are using the buffer (very common).

### Storage Buffers
Storage buffers allow load and store operations, including atomic operations handled by L2 cache.

### Texel Buffers
Texel buffers is a misnomer because are not their own buffer, rather they can be created from uniform or storage buffers. Texel buffers provide *formatted* views of a buffer. An example of formatting a buffer is, assuming the buffer contains floats, accessing the data in the form of `vec3` or `vec4` values.

The texel buffer is responsible for formatting the load, store, and atomic operations to the buffer. 

Data can be loaded from GPU memory into multiple L1 caches like with uniform buffers, but data can also be written to the storage buffer by shader programs. The GPU must be expected to handle race conditions w/ these write operations. They are (mostly) handled by atomic operations 

### Dynamic Buffer
Dynamic buffers are another form of buffers that can be created using uniform or storage buffers as the backing store. Dynamic buffers have offsets that can be dynamically changed. Offsets are used to change the amount of memory "allocated" at run-time, avoiding the cost of creating another buffer.

Particularly useful for dynamic buffers, it's possible for multiple `VkBuffer` instances to point to a single `VkMemory` object using different offsets.

### Inline Uniform Block
The inline uniform block (also known as a push constant) is block of memory that is allocated inline in the command buffer. It acts like uniform buffer but is not linked to a `VkMemory` instance since the memory for it is within the command buffer. This limits the total size of the inline uniform block.

### Memory
The memory system various across graphics devices (physical memory on board, unified memory, etc.). It's possible that there are multiple regions of GPU memory for different types of buffers. A block of memory is abstracted into a `VkMemory` object, which simply points to a region of GPU memory. A `VkBuffer` handle is a wrapper to a `VkMemory` object for buffer operations.

`VkMemory` and `VkBuffer` object must be created for uniform and storage buffers. For texel buffers, a `VkBufferView` object wraps a `VkBuffer` providing metadata on how to interpret the `VkMemory` provided by the `VkBuffer`.

All of these objects are stored in GPU memory, however `VkMemory` the programmer has some control over where and how much memory is allocated.  `VkBuffer` and `VkBufferView` objects are just returned by the Vulkan API to the programmer since they are small and simply store metadata.

## Image Types

### Storage Image
Very similar to a storage buffer that contains an image. Storage images support load, store, and atomic operations on images. Data can be loaded by multiple core clusters and multiple core clusters can store to the image. If the store occurs on separate instance of the image, then no synchronization is required.

Storage images are accessed through pixel coordinates, X and Y integer coordinates into the buffer.

### Sampled Images
Sampled images provide sampled, load-only access to an image. The difference is that sampled images are "sampled", i.e., accessed using normalized coordinates.

Normalized coordinates are coordinates where X and Y are floats within the range of \[-1, 1]. If the normalized coordinate falls within a pixel, then that pixel is chosen. If the normalized coordinate falls *in between* pixels, then the pixels are "sampled" - the neighboring pixel values are interpolated and that value is returned.

### Input Attachment
A type of image in Vulkan with load-only access within a render pass. The access to the input attachment is framebuffer local where only one pixel is accessed but no other pixels in the image.

### Memory
Images are stored using a similar mechanism to buffers. Instead of `VkBuffer` and `VkBufferView`, images use `VkImage` and `VkImageView` to reference a `VkMemory` instance.

The main difference between creating the processes b/w creating a buffer and creating an image is that the programmer must specify the swizzle of each of the components in an image.
## Descriptors
https://docs.vulkan.org/spec/latest/chapters/descriptorsets.html
A _descriptor_ is an opaque data structure representing a shader resource such as a buffer, buffer view, image view, sampler, or combined image sampler. Descriptors are organized into _descriptor sets_, which are bound during command recording for use in subsequent drawing commands. The arrangement of content in each descriptor set is determined by a _descriptor set layout_, which determines what descriptors can be stored within it. The sequence of descriptor set layouts that **can** be used by a pipeline is specified in a _pipeline layout_.

![[descriptor-set-architecture.png]]

## Pipeline Layout

The pipeline layout specifies the sequence of [[Graphics#Descriptors|descriptor sets]] and [[Graphics#Inline Uniform Block|push constants]] for the pipeline. The layout needs to be specified on pipeline creation and is used to determine the interface between shader stages and shader resources.

## Render Pass

In Vulkan graphics rendering is organized into render passes and subpasses.  A render pass is an abstraction over the graphics pipeline, therefore all rendering operations happen inside of a render pass. Render passes encapsulate the state of the render targets (i.e., framebuffer attachments) such as the size and format.

In Vulkan, a render pass is the set of attachments, the way they are used, and the rendering work that is performed using them.

### Subpass

https://developer.samsung.com/galaxy-gamedev/resources/articles/renderpasses.html
During normal rendering, it is not possible for a fragment shader to access the attachments to which it is currently rendering: GPUs have optimized hardware for writing to the attachments, and accessing the attachment interferes with this. However, some common rendering techniques such as deferred shading rely on being able to access the result of previous rendering during shading. For a tile-based renderer, the results of previous rendering can efficiently stay on-chip if subsequent rendering operations are at the same resolution, and if only the data in the pixel currently being rendered is needed (accessing different pixels may require access to values outside the current tile, which breaks this optimization). In order to help optimize deferred shading on tile-based renderers, Vulkan splits the rendering operations of a render pass into subpasses. All subpasses in a render pass share the same resolution and tile arrangement, and as a result, they can access the results of previous subpass.

In Vulkan, a render pass consists of one or more subpasses; for simple rendering operations, there may be only a single subpass in a render pass.

## Mipmaps

Imagine a scene with thousands of objects, each with an attached texture. There will be objects far away that have the same high resolution textures as objects closer to the camera. Since the objects are far away and probably only produce a few fragment, it would be difficult and wasteful to spend GPU time to pick a fragment from the high resolution texture.

Mipmaps are a collection of texture images where each subsequent image is twice as small as the previous one. The idea behind mipmaps is that, after distance thresholds, the graphics API will use a different mipmap texture for the distance of the object.

OpenGL is able to automatically create mipmaps for the programmer with :
```cpp
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
```

Vulkan is also able to generate mipmap levels by passing the mipmap levels when creating the texture and the image view, and using `VkCmdBlitImage` to generate the mipmaps.

## Depth Testing
Depth testing is simply: for every pixel on screen, record a value that says how far away from the camera it is. if we try to render anything further away than that for this pixel, discard it

# Graphics Techniques
## Phong Lighting Model

Phong lighting model is a model for approximating basic lighting in simple computer graphics programs. There are three components to phong shading:
1. **Ambient Lighting** - The illumination of an object from light that has been bounced around (i.e., reflection) from other objects in a scene. Note that ambient lighting does not actually calculate the color and intensity of these reflections (that would be global illumination). Rather, it approximates it by simply using a float to represent the overall strength of the effect
2. **Diffuse Lighting** - Diffuse lighting gives the object more brightness the closer its fragments are aligned to the light rays from a light source. This is done by finding the angle b/w the light ray and the fragment. Use the dot product b/w the light direction (calculated by subtracting the light and fragment positions) and the surface normal to find the angle b/w the two vectors. Multiplying this angle with the light color products a vector with the proper light intensity for that fragment.
	```cpp
	// fragment shader
	vec3 norm = normalize(Normal);
	vec3 lightDir = normalize(lightPos - FragPos);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * lightColor;
	```
3. **Specular Lighting** = Specular lighting is based on the light's direction vector and the object's normal vectors, but this time it is also based on the view direction e.g. from what direction the player is looking at the fragment. Specular lighting is based on the reflective properties of surfaces. If we think of the object's surface as a mirror, the specular lighting is the strongest wherever we would see the light reflected on the surface. Calculating the specular lighting is similar to diffuse lighting with a few differences. First, find the view direction by subtracting the view and fragment positions. Next, reflect the negative of the light direction around the normal vector. Find the dot product of the view and reflection direction vectors for the specular intensity.
	```cpp
	float specularStrength = 0.5;
	
	
	vec3 viewDir = normalize(viewPos - FragPos);
	vec3 reflectDir = reflect(-lightDir, norm);
	
	
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
	vec3 specular = specularStrength * spec * lightColor;  
	```

The results of the three stages is calculated in a specific order:
```cpp
vec3 result = (ambient + diffuse + specular) * objectColor;
FragColor = vec4(result, 1.0);
```
# Tools
- The Radeon GPU Profile
- Renderdoc (NonSemantic DebugInfo)
	- Where is my Draw? - Used for debugging a black screen - work through a series of checks to try to narrow down why a draw call is missing.
- Feature is called Debug Printf and is implemented in SPIR-V Tools optimizer and is an object in the VK_LAYER_KHRONOS_validation layer
# Questions
- When a GPU samples a texture, how does it pick which mipmap level to read from?
	GPU rasterizes everything in 2x2 fragment blocks, computes horizontal and vertical texture coordinate differences between fragments in the block
	
	The 2x2 quad has to execute shader instructions in lockstep, so that UV differences can be computed. Which leads to branching implications.
	
	2x2 quad rasterization means inefficiencies at small triangle sizes
- You are implementing a graphics API abstraction for an engine. How would it look like?
	**Resources**  
	These are simple data structures, without any functionality. They could be really simple, even plain old data (POD) types, for example descriptor structures or enums.
	
	Some resources are responsible of holding actual GPU data too. Using a `std::shared_ptr<void>` to manage internal state lets you be flexible to different graphics APIs (particularly DX12 and Vulkan)
	
	**Device**
	The purpose of resources is holding GPU data, but they don’t provide any functionality by themselves. The graphics device interface is responsible for that. There needs to be an abstraction for the device.
	
	The (graphics) device is responsible for allocation / creating the resources. For DX12 and Vulkan, this means abstracting away synchronization mechanisms to make sure that the resource is ready when the GPU needs to use it. There could also be an abstraction for an async wait operation 
	
	Graham Wihlidal's ["Halcyon Architecture](https://media.contentapi.ea.com/content/dam/ea/seed/presentations/wihlidal-halcyonarchitecture-notes.pdf)
	Egor Yusov's ["Designing a Modern Cross-Platform Low-Level Graphics Library"](https://www.gamasutra.com/blogs/EgorYusov/20171130/310274/Designing_a_Modern_CrossPlatform_LowLevel_Graphics_Library.php) 
- Mentorship opportunity?
	Aras Pranckevičius
	https://aras-p.info/blog/2019/01/07/Mentoring-You-Wont-Believe-What-Happened-Next/  

