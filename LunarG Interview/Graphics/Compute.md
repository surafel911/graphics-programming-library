# Compute Pipeline
- Run arbitrary code on the GPU
- Not limited by the fixed graphics pipeline
- Take advantage of extreme parallelism

Why use compute shaders instead?
- More low-level control
- Less dependencies
- More cross-platform
- Best for programs that interact with graphics

Compute Space:
Shader units are grouped into **work groups** and use GLSL with specific compute shader layouts and attribute

https://www.youtube.com/watch?v=KN9nHo9kvZs

https://vkguide.dev/docs/gpudriven/compute_shaders/
https://github.com/SaschaWillems/Vulkan/blob/master/examples/computeshader/computeshader.cpp