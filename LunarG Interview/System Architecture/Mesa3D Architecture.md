# Introduction
Mesa is a framework for user space drivers that provide an implementation for graphics APIs
* Has hardware vendor specific OpenGL/Vulkan & GLSL 
* Are considered [[Direct Rendering Infrastructure (DRI)]] drivers 
* Common [[Gallium3D]]  and [[NIR]] infrastructure

Mesa is a framework since much of the code is hardware agnostic. For example in OpenGL's state-based APIs many API calls do not have an immediate effect, they only modify values and those values are pushed to the hardware on draw calls. Since these APIs do not interact with the hardware, their implementation can be shared by multiple drivers, and each driver implements the draw call.

## How are APIs Implemented
Here is an example of a stack trace of a `glDrawArrays()` call.

```
`brw_upload_state () at brw_state_upload.c:651`
`brw_try_draw_prims () at brw_draw.c:483`
`brw_draw_prims () at brw_draw.c:578`
`vbo_draw_arrays () at vbo/vbo_exec_array.c:667`
`vbo_exec_DrawArrays () at vbo/vbo_exec_array.c:819`
`render () at main.cpp:363`
```

Notice `glDrawArrays()` is actually `vbo_draw_arrays()` and `vbo_draw_arrays()`. These are hardware independent functions and are reused by other drivers in Mesa. These generic functions are all over Mesa and would usually do things like checks for API errors, input formatting, any pre-processing for later stages, etc.

The `brw_draw_prims()` and `brw_upload_state()` are hooks in the DRI driver which call hardware-specific functionality such as uploading state, configuring shader stages, etc.


Hardware vendors provide an OpenGL implementation registers its hooks for context creating by assigning a global variable with the hooks they need. Here is an example in the Intel DRI driver:
```cpp
static const struct __DriverAPIRec brw_driver_api = {
   .InitScreen           = intelInitScreen2,
   .DestroyScreen        = intelDestroyScreen,
   .CreateContext        = brwCreateContext,
   .DestroyContext       = intelDestroyContext,
   .CreateBuffer         = intelCreateBuffer,
   .DestroyBuffer        = intelDestroyBuffer,
   .MakeCurrent          = intelMakeCurrent,
   .UnbindContext        = intelUnbindContext,
   .AllocateBuffer       = intelAllocateBuffer,
   .ReleaseBuffer        = intelReleaseBuffer
};

PUBLIC const __DRIextension **__driDriverGetExtensions_i965(void)
{
   globalDriverAPI = &brw_driver_api;

   return brw_driver_extensions;
}
```
Notice there are two types of hooks:
1. Hooks needed to link the driver not the DRI implementation (entry points for driver in Mesa)
2. Hooks related to the hardware implementation of OpenGL commands

Writing a new DRI driver is as simple as writing an implementation for all of these hooks. The rest are implemented in Mesa and reused across multiple devices.