
# Computer Architecture
https://ia902309.us.archive.org/21/items/c-05_20211009/C05.pdf

# Linux Kernel
https://www.linkedin.com/learning/advanced-linux-the-linux-kernel-2
https://sysdig.com/learn-cloud-native/container-security/understanding-linux-kernel/

## Kernel Modules

Kernel modules are pieces of code that can be loaded and unloaded into the kernel upon demand. They extend the functionality of the kernel without the need to reboot the system.

Custom codes can be added to Linux kernels via two methods.
- The basic way is to add the code to the kernel source tree and recompile the kernel.
- A more efficient way to do this is by adding code to the kernel while it is running. This process is called loading the module, where the module refers to the code that we want to add to the kernel.

Since we are loading these codes at runtime, and they are not part of the official Linux kernel, this is called **dynamic kernel modules support** (DKMS), which is different from the “base kernel”. The **base kernel** is located in the _/boot directory_ and is always loaded when we boot our machine, whereas DKMS modules are loaded after the base kernel is already loaded.

In the graphics stack, `amdgpu` is a DKMS module supplied by the kernel. It is not part of the Mesa project. 

## Linux Device Driver Types

In the traditional classification, there are three kinds of the device:

- Character device
- Block device
- Network device

In Linux, everything is a file. I mean Linux treats everything as a file even hardware.

### Character Device
A char file is a hardware file that reads/writes data in character by character fashion. Some classic examples are keyboard, mouse, and serial printer. If a user uses a char file for writing data, no other user can use the same char file to write data that blocks access to another user.

In the context of graphics, the GPU and displays are abstracted via a character device `/dev/dri/card0`. The first program to open this character device and open a connection is the **DRM master** and only the DRM master can write to the character device. All other programs that connect afterwards have read-only privileges.

### Block Device
A block file is a hardware file that reads/writes data in blocks instead of character by character. This type of file is very much useful when we want to write/read data in a bulk fashion. All our disks such are HDD, SSD, NVME, and USB.

This is the reason when we are formatting we consider block size. The writing of data is done in an asynchronous fashion, and it is CPU-intensive activity. These device files are used to store data on real hardware and can be mounted so that we can access the data we have written.

### Network Device
A network device is, so far as Linux’s network subsystem is concerned, an entity that sends and receives packets of data. This is normally a physical device such as an Ethernet card. Some network devices though are software only such as the loop back device which is used for sending data to yourself.

# Cache

Cache memory plays a crucial role in modern computer architecture. It is a small, high-speed memory that stores frequently accessed data and instructions, providing faster access compared to main memory. Cache memory consists of cache lines, which are fixed-size blocks that store a subset of the data present in main memory.

## Cache Coherence

Cache coherence refers to the consistency and synchronization of data stored in different caches within a multiprocessor or multi core system. In such systems, each processor or core typically has its own cache memory to improve performance. Cache memory plays a crucial role in computer architecture by providing fast access to frequently used data. However, maintaining data consistency across these private caches can be challenging, leading to the cache coherence problem.

### Cache Coherence Protocols
Cache coherence protocols are mechanisms designed to maintain cache coherence in multiprocessor systems. These protocols govern how caches communicate and coordinate their operations to ensure data consistency. Two commonly used cache coherence protocols are Snoopy Bus Protocols and Directory-Based Protocols.

#### Snoopy Bus Protocols
Snoopy Bus Protocols rely on a shared bus that connects all caches in a multiprocessor system. These protocols monitor the bus for memory transactions and employ snooping logic to maintain cache coherence.

1. Write-Invalidate Policy: In the Write-Invalidate policy, when a cache performs a write operation on a shared memory block, it invalidates or marks as invalid all other copies of that block in other caches. When a cache wants to read a memory block, it first checks if any other cache holds a modified copy. If so, it requests the updated data from the cache that has the latest copy or from main memory.

2. Write-Update Policy: In the Write-Update policy, when a cache performs a write operation on a shared memory block, it updates the value in its own cache and also updates all other copies of that block in other caches. This approach reduces the need for subsequent cache-to-cache transfers for read operations. However, it requires more bus bandwidth for broadcasting updates to all caches.

#### Directory-Based Protocols
Directory-Based Protocols use a centralized directory to maintain cache coherence information. The directory keeps track of which caches hold copies of each memory block and manages the permissions for accessing and modifying the blocks.

2. Common Directory for Maintaining Coherence: In Directory-Based Protocols, a central directory maintains the coherence information for all memory blocks. It keeps track of which caches have copies of each block and their respective states (e.g., shared, modified). Caches communicate with the directory to request permission for accessing and modifying memory blocks.

3. Permission-Based Data Sharing: Directory-Based Protocols manage data sharing through permissions. When a cache wants to read or write a memory block, it requests permission from the directory. The directory grants permission based on the current state of the block and the coherence protocol in use. Caches coordinate with the directory to ensure that they have the required permissions before performing operations on shared memory blocks.